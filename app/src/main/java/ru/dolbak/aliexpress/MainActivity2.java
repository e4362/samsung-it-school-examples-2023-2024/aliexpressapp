package ru.dolbak.aliexpress;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {
    String part1 = "", part2 = "";

    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        String name = getIntent().getStringExtra("name");
        String dob = getIntent().getStringExtra("dob");
        char letter = name.charAt(0);
        int dayOfBirth = Integer.parseInt(dob.substring(0, 2));

        String[] days = getResources().getStringArray(R.array.days);
        String[] names = getResources().getStringArray(R.array.names);

        for (String s : names){
            if (s.charAt(0) == letter){
                part1 = s.substring(1);
            }
        }
        part2 = days[dayOfBirth - 1];


        textView = findViewById(R.id.textView2);
        textView.setText(part1 + " " + part2);
    }

    public void openClip(View view) {
        String url = "https://www.youtube.com/watch?v=m5goQqnyeJ8";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    public void share(View view) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/html");
        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, "Я скачал приложение и узнал, " +
                "что на AliExpress меня бы продавали как " + part1 + " " + part2);
        startActivity(Intent.createChooser(sharingIntent, "Поделитесь о нашем приложении"));
    }
}